package ch.teko.oop.sascha.thekindcode

interface PersonRepository {
    fun createPerson(firstName: String, lastName: String): Person
    fun removePerson(firstName: String, lastName: String)
    fun getAllPersons(): List<Person>
    fun addSalutation(salutation: String, firstName: String, lastName: String): Person
}