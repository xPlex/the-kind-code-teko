package ch.teko.oop.sascha.thekindcode

class TheKindCode(
    val greeterView: CLI,
    val personRepository: PersonRepository
) {

    fun greet() {
        val greetings = arrayListOf<String>()
        val persons = personRepository.getAllPersons()
        persons.forEach { person ->
            greetings.add(person.greet())
        }
        greeterView.showGreet(greetings)
    }

    fun addPerson(firstName: String, lastName: String) {
        personRepository.createPerson(firstName, lastName)
        greeterView.showMenu(personRepository.getAllPersons())
    }

    fun removePerson(firstName: String, lastName: String) {
        personRepository.removePerson(firstName, lastName)
        greeterView.showMenu(personRepository.getAllPersons())
    }

    fun addSalutation(salutation: String, firstName: String, lastName: String) {
        personRepository.addSalutation(salutation, firstName, lastName)
        greeterView.showMenu(personRepository.getAllPersons())
    }

    fun getAllPerson() = personRepository.getAllPersons()
}
