package ch.teko.oop.sascha.thekindcode

import java.util.*

class CLI {
    private lateinit var controller: Controller
    private lateinit var theKindCode: TheKindCode

    fun setController(controller: Controller) {
        this.controller = controller
    }

    fun setGreeter(theKindCode: TheKindCode) {
        this.theKindCode = theKindCode
    }

    fun start() {
        val persons = theKindCode.getAllPerson()
        controller.parse("help")
        showMenu(persons)
        askForCommand()
    }

    fun showMenu(persons: List<Person>) {
        println("=============")
        persons.forEach { person ->
            println("\t$person")
        }
        println("=============")
    }

    fun showGreet(greetings: ArrayList<String>) {
        greetings.forEach { greeting ->
            println(greeting)
        }
    }

    private fun askForCommand() {
        val scanner = Scanner(System.`in`)
        var command: String
        do {
            command = scanner.nextLine()
        } while (controller.parse(command))
    }
}