package ch.teko.oop.sascha.thekindcode

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.list
import java.io.File

class PersonJsonRepository(val jsonPath: String) : PersonRepository {

    var persons: MutableList<Person>
    val json = Json(JsonConfiguration(prettyPrint = true))

    init {
        val file = File(jsonPath)
        if (file.exists()) {
            persons = json.parse(Person.serializer().list, file.readText()).toMutableList()
        } else {
            persons = mutableListOf()
            file.createNewFile()
            file.writeText(json.stringify(Person.serializer().list, persons))
        }
    }

    override fun createPerson(firstName: String, lastName: String): Person {
        val file = File(jsonPath)
        val createdPerson = Person(firstName, lastName)
        persons.add(createdPerson)
        file.writeText(json.stringify(Person.serializer().list, persons))
        return createdPerson
    }

    override fun removePerson(firstName: String, lastName: String) {
        val file = File(jsonPath)
        val personToRemove = Person(firstName, lastName)
        persons.remove(personToRemove)
        file.writeText(json.stringify(Person.serializer().list, persons))
    }

    override fun getAllPersons(): List<Person> {
        return persons.toList()
    }

    override fun addSalutation(salutation: String, firstName: String, lastName: String): Person {
        val file = File(jsonPath)

        for(person in persons) {
            if(person.getFirstName() == firstName && person.getLastName() == lastName) {
                person.setSalutation(salutation)
                file.writeText(json.stringify(Person.serializer().list, persons))
                return person
            }
        }

        val createdPerson = Person(firstName, lastName)
        createdPerson.setSalutation(salutation)
        persons.add(createdPerson)
        file.writeText(json.stringify(Person.serializer().list, persons))
        return createdPerson
    }
}