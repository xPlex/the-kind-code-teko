package ch.teko.oop.sascha.thekindcode

const val jsonPath = "C:/Users/sasch/OneDrive/Desktop/greeter-teko2020-master/src/main/resources/repo/person.json"

fun main(args: Array<String>) {
    val cli = CLI()
    val repository = PersonJsonRepository(jsonPath)
//    val repository = PersonInMemoryRepository()
    val kindCode = TheKindCode(cli, repository)
    val controller = Controller(kindCode)
    cli.setController(controller)
    cli.setGreeter(kindCode)
    cli.start()
}