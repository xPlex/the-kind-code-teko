package ch.teko.oop.sascha.thekindcode

class Controller(val theKindCode: TheKindCode) {
    fun parse(command: String): Boolean {
        if ("greet" == command) {
            theKindCode.greet()
            return true
        } else if (command.startsWith("add ")) {
            val commandList: List<String> = getSpaceSplittedCommand(command)
            theKindCode.addPerson(commandList[1], commandList[2])
            return true
        } else if (command.startsWith("remove ")) {
            val commandList: List<String> = getSpaceSplittedCommand(command)
            theKindCode.removePerson(commandList[1], commandList[2])
            return true
        } else if (command.startsWith("salutation ")) {
            val commandList: List<String> = getSpaceSplittedCommand(command)
            theKindCode.addSalutation(commandList[1], commandList[2], commandList[3])
            return true
        } else if ("exit" == command) {
            return false
        } else {
            println("You can use following commands: \n\t- add [firstName] [lastName]\n\t- remove [firstName] [lastName]\n\t- salutation [salutation] [firstName] [lastName]\n\t- greet\n\t- exit\n\t- help")
            return true
        }
    }

    /*private fun getFirstName(command: String): String {
        return command.split(" ")[1]
    }
    private fun getLastName(command: String): String {
        return command.split(" ")[2]
    }*/

    private fun getSpaceSplittedCommand(command: String): List<String> {
        return command.split(" ")
    }
}