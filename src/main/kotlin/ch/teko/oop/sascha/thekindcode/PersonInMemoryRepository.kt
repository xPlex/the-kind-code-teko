package ch.teko.oop.sascha.thekindcode

class PersonInMemoryRepository : PersonRepository {
    val persons = arrayListOf<Person>()
    override fun createPerson(firstName: String, lastName: String): Person {
        val newPerson = Person(firstName, lastName)
        persons.add(newPerson)
        return newPerson
    }

    override fun removePerson(firstName: String, lastName: String) {
        val personToRemove = Person(firstName, lastName)
        persons.remove(personToRemove)
    }

    override fun getAllPersons(): List<Person> {
        return persons
    }

    override fun addSalutation(salutation: String, firstName: String, lastName: String): Person {
        for (person in persons) {
            if (person.getFirstName() == firstName && person.getLastName() == lastName) {
                person.setSalutation(salutation)
                return person
            }
        }

        val newPerson = Person(firstName, lastName)
        newPerson.setSalutation(salutation)
        persons.add(newPerson)
        return newPerson
    }
}