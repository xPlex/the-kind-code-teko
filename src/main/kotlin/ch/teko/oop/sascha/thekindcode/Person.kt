package ch.teko.oop.sascha.thekindcode

import kotlinx.serialization.Serializable

@Serializable
data class Person(private val firstName: String, private val lastName: String) {
    private var salutation: String = "";

    fun greet(): String {
        if (salutation == "") {
            return "Hello, $firstName $lastName"
        } else {
            return "Hello, $salutation $firstName $lastName"
        }
    }

    fun setSalutation(salutation: String) {
        this.salutation = salutation
    }

    fun getFirstName(): String {
        return this.firstName
    }

    fun getLastName(): String {
        return this.lastName
    }

    override fun toString(): String {
        return "Person(firstName='$firstName', lastName='$lastName', salutation='$salutation')"
    }


}